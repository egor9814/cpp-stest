//
// Created by egor9814 on 02 Aug 2021.
//

#define TestVisibleEnabled
#include <stest.hpp>

AutoTestUnit(TestGroupOne)

	class PrivateVisibility {
		int p1{1};
		float p2{2};

		[[nodiscard]] int f1(int a, int b) const {
			return a * b + p1;
		}

	public:
		bool p3{true};
		char p4{'X'};

		TestVisible(PrivateVisibility)
			TestConstProperty(p1)
			TestProperty(p2)
			TestMethod(f1)
		TestVisibleEnd()
	};

	AutoTestCase(PrivateVisibility) {
		TestMessage("Just test message");
		PrivateVisibility pv;
		pv.p3 = false; // public visibility
		TestCheckMessage(pv.p4 == 'X', "Check initial state");
		pv.p4 = 'Y';   // public visibility
		TestCheckMessage(pv.p4 == 'Y', "Check new state");

		TestVisibleResolve(PrivateVisibility) r(pv); // amazing hook!
		auto &pv2 = *r;
		// pv2.p1() = 10; // error: visible, but it is const
		TestCheckMessage(pv2.p2() == 2.0F, "Check initial private state");
		pv2.p2() = 3.14F; // i can change this
		TestCheckMessage(pv2.p2() == 3.14F, "Check new private state");

		TestRequire(43 == pv2.f1()(6, 7));
	}

EndTestUnit(TestGroupOne)
