//
// Created by egor9814 on 02 Aug 2021.
//

#ifndef STEST_MACRO_HPP
#define STEST_MACRO_HPP

#ifdef TestVisibleEnabled
#define TestVisible(ClassName)\
friend class ru::egor9814::stest::TestVisibleResolver<ClassName>;\
class __TestVisibleResolver__;\
friend class __TestVisibleResolver__;\
class __TestVisibleResolver__ : public __private__::ru::egor9814::stest::BaseResolver<ClassName> {\
	using BaseResolver<ClassName>::ClassType;\
	ClassName *ptr;\
	friend class ru::egor9814::stest::TestVisibleResolver<ClassName>;\
	explicit __TestVisibleResolver__(ClassName *ptr) : ptr(ptr) {}\
public:

#define TestMethod(MethodName)\
auto MethodName() -> Function<decltype(&ClassType::MethodName)> {\
	return Function<decltype(&ClassType::MethodName)>(ptr, &ClassType::MethodName);\
}

#define TestProperty(PropertyName)\
auto PropertyName() -> decltype(ClassType::PropertyName)& {\
	return ptr->PropertyName;\
}

#define TestConstProperty(PropertyName)\
auto PropertyName() const -> const decltype(ClassType::PropertyName)& {\
	return ptr->PropertyName;\
}

#define TestVisibleEnd() };

#define TestVisibleResolve(ClassName) ru::egor9814::stest::TestVisibleResolver<ClassName>

#else // TestVisibleEnabled

#define TestVisible(ClassName)
#define TestMethod(FunctionName)
#define TestProperty(PropertyName)
#define TestConstProperty(PropertyName)
#define TestVisibleEnd()
#define TestVisibleResolve(ClassName) ru::egor9814::stest::TestVisibleResolver<void>

#endif // TestVisibleEnabled


#define __TestUnitContent__(UnitName, FixtureType)\
    using __UnitTestFixtureType__ = FixtureType;\
    struct UnitName##_TestRegistrar : public ::__private__::ru::egor9814::stest::TestRegistrar {\
        UnitName##_TestRegistrar();\
        static UnitName##_TestRegistrar& getInstance() { static UnitName##_TestRegistrar tr; return tr; }\
    };\
    inline void UnitName##_Execute(int argc, char **argv) {\
        UnitName##_TestRegistrar::getInstance().execute(argc, argv);\
    }\
    inline UnitName##_TestRegistrar::UnitName##_TestRegistrar() : TestRegistrar(\
    		L"\tentering to '", L"'",                   \
    		L"\texit from '", L"'"\
	) {\
        ::__private__::ru::egor9814::stest::registerUnitTest(\
			L"" #UnitName, [](int a, char **b){ UnitName##_Execute(a, b); });\
    }\
    using TestRegistrarInstnaceType = UnitName##_TestRegistrar;

#define AutoTestUnit(UnitName)\
namespace UnitName {\
    struct __UnitTestFixture_Null__{};\
    __TestUnitContent__(UnitName, __UnitTestFixture_Null__)

#define FixtureTestUnit(UnitName, FixtureClass)\
namespace UnitName {\
    __TestUnitContent__(UnitName, FixtureClass)

#define EndTestUnit(UnitName) } /* UnitName */


#define __TestCaseContent__(TestCaseName)\
        void test();\
        TestCaseName() : testName(L"" #TestCaseName) { TestRegistrarInstnaceType::getInstance().registerTest(testName.data(), [this](int,char*[]){ this->test(); }); }\
    private:\
        std::wstring testName;\
    public:\
        const std::wstring& getTestName() const { return testName; }\
    };\
    static TestCaseName TestCaseName##Instance;\
    inline void TestCaseName::test()

#define AutoTestCase(TestCaseName)\
    struct TestCaseName##Test : public __UnitTestFixtureType__ {\
        __TestCaseContent__(TestCaseName##Test)


#define FixtureTestCase(TestCaseName, FixtureClass)\
    struct TestCaseName##Test : public __UnitTestFixtureType__, public FixtureClass {\
        __TestCaseContent__(TestCaseName##Test)


#define UnitTestMain()\
struct __UnitTestRegistrarInstanceImpl__ {\
    static __UnitTestRegistrarInstanceImpl__& getInstance() { static __UnitTestRegistrarInstanceImpl__ impl; return impl; }\
    __private__::ru::egor9814::stest::TestRegistrar registrar;\
    __UnitTestRegistrarInstanceImpl__() : registrar(L"\nstarting unit '", L"'", L"finishing unit '", L"'") {}\
};\
void __private__::ru::egor9814::stest::registerUnitTest(const wchar_t *name, std::function<void(int, char **)> &&f) {\
    __UnitTestRegistrarInstanceImpl__::getInstance().registrar.registerTest(name, std::move(f));\
}\
int main(int argc, char **argv) {\
    __UnitTestRegistrarInstanceImpl__::getInstance().registrar.execute(argc, argv);\
    auto errors = __private__::ru::egor9814::stest::TestRegistrar::getErrorsCount();\
    if (errors) std::wcout << L"errors count: " << errors << std::endl;\
    else std::wcout << std::endl << L"all tests passed!" << std::endl;\
    return 0;\
}


#define TestCheck(Value)\
    __private__::ru::egor9814::stest::CheckStructure(__FILE__, __LINE__, std::wstring(L"" #Value))\
    .check(Value, false)

#define TestCheckMessage(Value, Message)\
    __private__::ru::egor9814::stest::CheckStructure(__FILE__, __LINE__, std::wstring(L"" #Value), Message)\
    .check(Value, false)

#define TestMessage(Message)\
    __private__::ru::egor9814::stest::CheckStructure(__FILE__, __LINE__, std::wstring(L""), Message)\
    .check(true, false)

#define TestRequire(Value)\
    if (!__private__::ru::egor9814::stest::CheckStructure(__FILE__, __LINE__, std::wstring(L"" #Value))\
    .check(Value, true)) return

#define TestRequireMessage(Value, Message)\
    if (!__private__::ru::egor9814::stest::CheckStructure(__FILE__, __LINE__, std::wstring(L"" #Value), Message)\
    .check(Value, true)) return


#define DisableTest() do { TestMessage(L"test '" + getTestName() + L"' disabled"); return; } while(false)

#endif //STEST_MACRO_HPP
