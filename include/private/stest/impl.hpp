//
// Created by egor9814 on 02 Aug 2021.
//

#ifndef STEST_IMPL_HPP
#define STEST_IMPL_HPP

#include <set>
#include <list>
#include <atomic>
#include <memory>
#include <string>
#include <vector>
#include <iostream>
#include <functional>

namespace __private__::ru::egor9814::stest {

	using test_function_t = std::function<void(int,char**)>;

	void registerUnitTest(const wchar_t *name, test_function_t &&f);

	class TestRegistrar {
		friend class CheckStructure;

		static std::atomic<unsigned long> &totalErrorsCount();
		static std::atomic<unsigned long> &totalFatalErrorsCount();

		struct TestFunctionHolder {
			std::wstring name;
			test_function_t f;

			TestFunctionHolder(std::wstring name, test_function_t &&f)
					: name(std::move(name)), f(std::move(f)) {}

			void run(int argc, char **argv) const {
				f(argc, argv);
			}

			bool operator==(const TestFunctionHolder& t) const {
				return name == t.name;
			}

			bool operator!=(const TestFunctionHolder& t) const {
				return name != t.name;
			}

			bool operator<(const TestFunctionHolder& t) const {
				return name < t.name;
			}

			bool operator>(const TestFunctionHolder& t) const {
				return name > t.name;
			}

			bool operator<=(const TestFunctionHolder& t) const {
				return name <= t.name;
			}

			bool operator>=(const TestFunctionHolder& t) const {
				return name >= t.name;
			}

			bool operator==(const std::wstring& t) const {
				return name == t;
			}

			bool operator!=(const std::wstring& t) const {
				return name != t;
			}

			bool operator<(const std::wstring& t) const {
				return name < t;
			}

			bool operator>(const std::wstring& t) const {
				return name > t;
			}

			bool operator<=(const std::wstring& t) const {
				return name <= t;
			}

			bool operator>=(const std::wstring& t) const {
				return name >= t;
			}
		};

		std::set<std::wstring> registeredTests{};
		std::list<TestFunctionHolder> tests{};
		std::wstring enterMessagePrefix, enterMessagePostfix;
		std::wstring exitMessagePrefix, exitMessagePostfix;


		std::wostream &enter(std::wostream& out, const std::wstring& msg);

		std::wostream &exit(std::wostream& out, const std::wstring& msg);

	public:
		explicit TestRegistrar(std::wstring enterMessagePrefix = L"enter test '",
							   std::wstring enterMessagePostfix = L"'",
							   std::wstring exitMessagePrefix = L"exit test '",
							   std::wstring exitMessagePostfix = L"'");

		void registerTest(const wchar_t *name, test_function_t &&f);

		void execute();

		void execute(int argc, char **argv);

		static size_t getErrorsCount();
	};


	class CheckStructure {
		std::wstring file, text, message;
		int line;

	public:
		CheckStructure(const char *file, int line, std::wstring text, std::wstring message = L"");

		CheckStructure(const char *file, int line, std::wstring text, const std::string &message);

		bool check(bool value, bool required);
	};


	template <typename T>
	class BaseResolver {
	public:
		using ClassType = T;

		template <typename Signature> class Function;

		template <typename Obj, typename Ret, typename ... Args>
		class Function<Ret (Obj::*) (Args...)> {
		public:
			using Object = Obj;
			using ReturnType = Ret;
			using FunctionType = Ret (Obj::*) (Args...);

			static ReturnType call(FunctionType f, Object *o, Args &&...args) {
				if constexpr(std::is_void<ReturnType>::value) {
					(o->*f)(args...);
				} else {
					return (o->*f)(args...);
				}
			}

		private:
			Obj *ptr;
			Ret (Obj::*f) (Args...);

		public:
			Function(Object *ptr, ReturnType (Object::*f) (Args...)) : ptr(ptr), f(f) {}

			Ret operator()(Args &&...args) {
				if constexpr(std::is_void<ReturnType>::value) {
					call(f, ptr, std::forward<Args>(args)...);
				} else {
					return call(f, ptr, std::forward<Args>(args)...);
				}
			}
		};

		template <typename Obj, typename Ret, typename ... Args>
		class Function<Ret (Obj::*) (Args...) const> {
		public:
			using Object = Obj;
			using ReturnType = Ret;
			using FunctionType = Ret (Obj::*) (Args...) const;

			static ReturnType call(FunctionType f, Object *o, Args &&...args) {
				if constexpr(std::is_void<ReturnType>::value) {
					(o->*f)(args...);
				} else {
					return (o->*f)(args...);
				}
			}

		private:
			Obj *ptr;
			Ret (Obj::*f) (Args...) const;

		public:
			Function(Object *ptr, ReturnType (Object::*f) (Args...) const) : ptr(ptr), f(f) {}

			Ret operator()(Args &&...args) const {
				if constexpr(std::is_void<ReturnType>::value) {
					call(f, ptr, std::forward<Args>(args)...);
				} else {
					return call(f, ptr, std::forward<Args>(args)...);
				}
			}
		};

	};

} //__private__::ru::egor9814::stest

namespace ru::egor9814::stest {

	template <typename T>
	class TestVisibleResolver {
	public:
		using Resolver = typename T::__TestVisibleResolver__;
	private:
		Resolver r;
	public:
		explicit TestVisibleResolver(T &instance) : r(&instance) {}

		Resolver &get() {
			return r;
		}

		Resolver &operator*() {
			return r;
		}

		Resolver *operator->() {
			return &r;
		}
	};

	template <> class TestVisibleResolver<void> {
	public:
		template <typename T> explicit TestVisibleResolver(T&) {}
	};

}

#endif //STEST_IMPL_HPP
