//
// Created by egor9814 on 02 Aug 2021.
//

#include <private/stest/impl.hpp>
#include <iostream>
#include "strings.hpp"

namespace __private__::ru::egor9814::stest {

	std::atomic<unsigned long> &TestRegistrar::totalErrorsCount() {
		static std::atomic<unsigned long> count{0};
		return count;
	}

	std::atomic<unsigned long> &TestRegistrar::totalFatalErrorsCount() {
		static std::atomic<unsigned long> count{0};
		return count;
	}

	std::wostream &TestRegistrar::enter(std::wostream &out, const std::wstring &msg) {
		return out << enterMessagePrefix << msg << enterMessagePostfix;
	}

	std::wostream &TestRegistrar::exit(std::wostream &out, const std::wstring &msg) {
		return out << exitMessagePrefix << msg << exitMessagePostfix;
	}

	TestRegistrar::TestRegistrar(
			std::wstring enterMessagePrefix,
			std::wstring enterMessagePostfix,
			std::wstring exitMessagePrefix,
			std::wstring exitMessagePostfix
			) : enterMessagePrefix(std::move(enterMessagePrefix)),
				enterMessagePostfix(std::move(enterMessagePostfix)),
				exitMessagePrefix(std::move(exitMessagePrefix)),
				exitMessagePostfix(std::move(exitMessagePostfix)) {}

	void TestRegistrar::registerTest(const wchar_t *name, test_function_t &&f) {
		std::wstring testName(name);
		if (registeredTests.contains(testName))
			return;
		TestFunctionHolder t(testName, std::move(f));
		registeredTests.insert(testName);
		tests.push_back(t);
	}

	void TestRegistrar::execute() {
		for (auto &it : tests) {
			enter(std::wcout, it.name) << std::endl;
			it.f(0, nullptr);
			exit(std::wcout, it.name) << std::endl;
			if (totalFatalErrorsCount()) break;
		}
	}

	void TestRegistrar::execute(int argc, char **argv) {
		if (argc > 1) {
			std::wstring testFormat;
			static const std::string testArg("-t");

			for(int i = 1; i < argc; i++) {
				if (testArg == argv[i] && i+1 < argc) {
					testFormat = toWString(argv[++i]);
				}
			}

			if (!testFormat.empty()) {
				auto pos = testFormat.find_first_of('/');
				if (pos != std::string::npos) {
					auto testName = testFormat.substr(0, pos);
					if (!registeredTests.count(testName))
						return;

					std::vector<char*> new_argv;
					new_argv.reserve(3);
					new_argv.push_back(argv[0]);
					new_argv.push_back(const_cast<char*>("-t"));
					new_argv.push_back(const_cast<char*>(toString(testFormat.substr(pos+1)).data()));

					for (auto &it : tests) {
						if (it == testName) {
							it.run(3, new_argv.data());
							break;
						}
					}
					return;
				}
				if (testFormat != L"*") {
					if (registeredTests.contains(testFormat)) {
						for (auto &it : tests) {
							if (it == testFormat) {
								it.run(1, argv);
								break;
							}
						}
					}
					return;
				}
			}
		}
		execute();
	}

	size_t TestRegistrar::getErrorsCount() {
		return static_cast<size_t>(totalErrorsCount().load() + totalFatalErrorsCount().load());
	}

}
