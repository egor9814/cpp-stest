//
// Created by egor9814 on 02 Aug 2021.
//

#ifndef STEST_STRINGS_HPP
#define STEST_STRINGS_HPP

#include <string>

std::wstring toWString(const char *str);

std::wstring toWString(const std::string &str);

std::string toString(const std::wstring &str);

#endif //STEST_STRINGS_HPP
