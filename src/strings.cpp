//
// Created by egor9814 on 02 Aug 2021.
//

#include "strings.hpp"
#include <codecvt>
#include <locale>

std::wstring toWString(const char *str) {
	using namespace std;
	using convert_type = codecvt_utf8<wchar_t>;
	wstring_convert<convert_type, wchar_t> converter;
	return converter.from_bytes(str);
}

std::wstring toWString(const std::string &str) {
	using namespace std;
	using convert_type = codecvt_utf8<wchar_t>;
	wstring_convert<convert_type, wchar_t> converter;
	return converter.from_bytes(str);
}

std::string toString(const std::wstring &str) {
	using namespace std;
	using convert_type = codecvt_utf8<wchar_t>;
	wstring_convert<convert_type, wchar_t> converter;
	return converter.to_bytes(str);
}
