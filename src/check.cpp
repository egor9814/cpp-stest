//
// Created by egor9814 on 02 Aug 2021.
//

#include <private/stest/impl.hpp>
#include <iostream>
#include "strings.hpp"

namespace __private__::ru::egor9814::stest {

	CheckStructure::CheckStructure(const char *file, int line, std::wstring text, std::wstring message)
			: file(toWString(file)), line(line), text(std::move(text)), message(std::move(message)) {}

	CheckStructure::CheckStructure(const char *file, int line, std::wstring text,
								   const std::string &message)
			: file(toWString(file)), line(line), text(std::move(text)), message(toWString(message)) {}

	bool CheckStructure::check(bool value, bool required) {
		std::wcout << (value ? L'I' : L'E') << L": " << file << L":" << line;
		if (!text.empty()) {
			std::wcout << L"> '" << text << L"'";
			if (value) {
				std::wcout << L" has passed.";
			} else {
				(required ? TestRegistrar::totalFatalErrorsCount() : TestRegistrar::totalErrorsCount())++;
				std::wcout << L" is failed.";
				if (!message.empty()) {
					std::wcout << L' ' << message;
				}
			}
		} else if (!message.empty()) {
			std::wcout << L"> " << message;
		}
		std::wcout << std::endl;
		return !required || value;
	}

}
